# Estudio sobre el tratamiento periodístico de los artículos sobre inmigración y refugiados.
Análisis de texto de más de 23.000 artículos de prensa sobre refugiados e inmigración publicados entre 2010 y 2018 en El País, El Mundo, ABC, Público y El Diario. El resultado final del proyecto es este [informe](), este [dataset](https://gitlab.com/hmeleiros/refugiados/blob/master/shiny/refugiados/data/cincomedios_def.csv), y este [visualizador](https://meleiro.shinyapps.io/refugiados/).



## Metodología

Para hacer este análisis he scrapeado las webs de El País, El Mundo, ABC, ElDiario.es y Público.es en busca de artículos relacionados con la inmigración y los refugiados. Para ello he usado los buscadores de los periódicos introduciendo como términos de búsqueda las palabras 'inmigracion' y 'refugiados'. Debido a problemas técnicos no he podido usar el buscador de El País, por lo que para este caso he usado el sistema de etiquetas y he scrapeado las siguientes: 'crisis migratoria europea', 'crisis migratoria', 'crisis humanitaria', 'refugiados', 'política migración' y 'migracion'. Los scripts para hacer estos scrapeos los puedes encontrar [aquí](https://gitlab.com/hmeleiros/refugiados).

Posteriormente he unido los resultados de cada periódico. Por último, y debido a que los buscadores de los periódicos dan con artículos que o bien no tienen que ver con el tema o tienen que ver con él de una forma solo tangencial, he creado diferentes variables en el dataset contando el número de veces que aparecen las palabras de un campo semántico definido para el tema en cuestión y he delimitado un umbral para descartar falsos positivos. El código para hacer estos dos últimos pasos lo puedes encontrar en [este script](https://gitlab.com/hmeleiros/refugiados/blob/master/02-merge%20and%20prepare%20for%20Open%20Refine.R).

Para limpiar el campo de autor he usado [Open Refine](http://openrefine.org/).

Para obtener el dataset resultante (formateado, limpio y sin falsos positivos) con los más de 23.000 artículos descárgalo [aquí](https://gitlab.com/hmeleiros/refugiados/blob/master/shiny/refugiados/data/cincomedios_def.csv), descarga el repositorio entero como zip o clónalo desde la terminal usando Git. Si no tienes [Git](https://es.wikipedia.org/wiki/Git) instalado sigue [estas instrucciones](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

Si estás interesado en ver el código del [visualizador del dataset](https://meleiro.shinyapps.io/refugiados/) sigue [este link](https://gitlab.com/hmeleiros/refugiados/tree/master/shiny/refugiados).


## Cómo usar los scrapers

Para facilitar el uso de los scrapers he creado un shell script para unix que consiste en la ejecución en linea de cada uno de los scripts de R para cada periódico. De tal forma que con descargarse el repositorio y ejecutar desde la terminal los siguientes comandos debería ponerse todo en marcha:

```
## Abre la terminal
## Sitúate en el repositorio descargado
## Ejecuta el siguiente comando

./delTiron.sh

## Para hacer el merge de los datasets resultantes ejecuta lo siguiente

chmod +x merge.R
Rscript merge.R

```

Si se prefiere usar los scrapers individualmente en Rstudio hay que modificar (al principio de cada script) el directorio de trabajo y las rutas donde se van a escribir los csv.

```
### IMPORTANTE: establecer el directorio de trabajo y las rutas para el csv del indice y de la extraccion
#setwd("/RUTA/A/UN/DIRECTORIO/EN/TU/ORDENADOR/")   ####### CAMBIAR ESTO A UN DIRECTORIO EXISTENTE EN TU ORDENADOR

ruta.indice <- "data/indicePublico.csv"            ####### ASEGÚRATE DE CAMBIAR ESTO O DE QUE EXISTA LA CARPETA 'data' EN EL DIRECTORIO DE TRABAJO
ruta.extraccion <- "data/articulosPublico.csv"
```

## Cómo citar

Para citar el dataset:

[Meleiro Suárez, H.](https://twitter.com/hmeleiros) (2018). Imigración y crisis de los refugiados en cinco periódicos españoles [dataset]. URL: https://github.com/meneos/refugiados

Para citar los scrapers:

[Meleiro Suárez, H.](https://twitter.com/hmeleiros) (2018). Scrapeando cinco periódicos digitales españoles [repositorio]. URL: https://github.com/meneos/refugiados


